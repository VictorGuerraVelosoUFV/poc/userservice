import sys

import flask
from flask_restplus import abort, Namespace

from app import Resource, request, db
from http import HTTPStatus

from auth.authenticate import authenticate, authenticate_or_abort

evento_ns = Namespace(name="evento", description="Rotas relacionadas a participação e administração em eventos",
                      path="/evento")


@evento_ns.route("/<id_evento>")
class EventoResource(Resource):
    def delete(self, id_evento):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{uid}"}) is None:
            abort(HTTPStatus.UNAUTHORIZED)
        participantes = db.participacao.delete_many({"Evento": f"{id_evento}"}).deleted_count
        admins = db.administracao.delete_many({"Evento": f"{id_evento}"}).deleted_count
        print(participantes + admins)


@evento_ns.route("/<id_evento>/participante")
class ParticipantesResource(Resource):
    def get(self, id_evento):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        participantes = db.participacao.find({"Evento": f"{id_evento}"})
        output = []
        for p in participantes:
            p["_id"] = str(p["_id"])
            output.append(p)
        return output


@evento_ns.route("/<id_evento>/admin")
class AdminsResource(Resource):
    def get(self, id_evento):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{uid}"}) is None:
            abort(HTTPStatus.UNAUTHORIZED)
        admins = db.administracao.find({"Evento": f"{id_evento}"})
        output = []
        for adm in admins:
            adm["_id"] = str(adm["_id"])
            output.append(adm)
        return output


@evento_ns.route("/<id_evento>/participante/<id>")
class ParticipanteResource(Resource):
    def get(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if id != uid:
            abort(HTTPStatus.UNAUTHORIZED)
        participante = db.participacao.find_one({"Evento": f"{id_evento}", "Participante": f"{id}"})
        if participante is None:
            abort(HTTPStatus.NOT_FOUND)
        participante["_id"] = str(participante["_id"])
        return participante

    def post(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if id != uid:
            abort(HTTPStatus.UNAUTHORIZED)
        participante = db.participacao.find_one({"Evento": f"{id_evento}", "Participante": f"{id}"})
        if participante is None:
            print(db.participacao.insert_one({"Evento": f"{id_evento}", "Participante": f"{id}"}).inserted_id)
        else:
            print(participante["_id"])

    def delete(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if id != uid:
            abort(HTTPStatus.UNAUTHORIZED)
        deletado = db.participacao.delete_one({"Evento": f"{id_evento}", "Participante": f"{id}"}).deleted_count
        print(deletado)


@evento_ns.route("/<id_evento>/admin/<id>")
class AdminResource(Resource):
    def get(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{uid}"}) is None or id != uid:
            abort(HTTPStatus.UNAUTHORIZED)
        admin = db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{id}"})
        if admin is None:
            abort(HTTPStatus.NOT_FOUND)
        admin["_id"] = str(admin["_id"])
        return admin

    def post(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if len(list(db.administracao.find({"Evento": f"{id_evento}"}))) == 0:  # First admin
            admin = db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{id}"})  # ja existe?
            if admin is None:  # nao?
                print(db.administracao.insert_one({'Evento': f"{id_evento}", "Admin": f"{id}"}).inserted_id)
            else:  # ja?
                print(admin["_id"])
        else:  # New admin
            if db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{uid}"}) is None:
                abort(HTTPStatus.UNAUTHORIZED)
            else:
                print(db.administracao.insert_one({'Evento': f"{id_evento}", "Admin": f"{id}"}).inserted_id)

    def delete(self, id_evento, id):
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        if db.administracao.find_one({"Evento": f"{id_evento}", "Admin": f"{uid}"}) is None:
            abort(HTTPStatus.UNAUTHORIZED)
        elif len(list(db.administracao.find({"Evento": f"{id_evento}"}))) == 1:
            abort(HTTPStatus.UNAUTHORIZED)
        deletado = db.administracao.delete_one({"Evento": f"{id_evento}", "Admin": f"{id}"}).deleted_count
        print(deletado)
