from http import HTTPStatus

import requests


def authenticate(base_url="poc.primary157.com.br:82", **kwargs):
    assert kwargs["access_token"] is not None
    assert kwargs["uid"] is not None
    response = requests.get(f"http://{base_url}/api/auth_users/{kwargs['uid']}/?access_token={kwargs['access_token']}")
    print(response)
    if response.ok:
        return response.json()
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(
            f"Authentication Failed: wrong access_token ({kwargs['access_token']}) and uid({kwargs['uid']}) pair!")
    else:
        raise RuntimeError("Something went wrong during authentication!")


def authenticate_or_abort(get, abort):
    try:
        token = get("access_token")
        uid = get("uid")
        print(f"Token: {token}\nUID: {uid}")
        _ = authenticate(access_token=token, uid=uid)
        return token, uid
    except (KeyError, AssertionError):
        abort(HTTPStatus.UNAUTHORIZED, "Missing access_token and/or uid Query Params")
    except ValueError:
        abort(HTTPStatus.UNAUTHORIZED, "Invalid access_token/uid combination")
    except RuntimeError:
        abort(HTTPStatus.INTERNAL_SERVER_ERROR)
