from flask import Flask, request
from flask_restplus import Resource, Api, fields
from pymongo import MongoClient
import os

app = Flask(__name__)

client = MongoClient(f"mongodb://{os.getenv('MONGO_URI', 'user-mongo')}:27017/")
db = client.mymobiconf

api = Api(app)


def register_namespaces():
    from resources import evento_ns

    api.add_namespace(evento_ns)


register_namespaces()
