from random import uniform

from app import app
from http import HTTPStatus

import requests

credentials = {
    "username": "string",
    "password": "string"
}


def gen_string(size):
    import string
    import random
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))


def test_participacao_fake_auth():
    evento_id = int(uniform(5, 1000))
    fake_auth = {'id': gen_string(64), 'userId': gen_string(36)}
    with app.test_client() as c:
        rv = c.get(f"/evento/{evento_id}/participante?access_token={fake_auth['id']}&uid={fake_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Non-existent user allowed"

        rv = c.post(f"/evento/{evento_id}/participante/{fake_auth['userId']}?access_token={fake_auth['id']}&uid={fake_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Non-existent user allowed"


def test_participacao_crud():
    another_credentials = {
        "username": "victorgv",
        "password": "string"
    }
    new_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=another_credentials).json().items() if key in ['id', 'userId']}

    evento_id = int(uniform(5, 1000))
    user_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items() if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.get(f"/evento/{evento_id}/participante?access_token={user_auth['id']}&uid={user_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "User prohibited from getting public information"

        rv = c.post(f"/evento/{evento_id}/participante/{user_auth['userId']}?access_token={user_auth['id']}&uid={user_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "User prohibited from subscribe itself"

        rv = c.post(f"/evento/{evento_id}/participante/{new_auth['userId']}?access_token={new_auth['id']}&uid={new_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "User prohibited from subscribe itself"

        rv = c.post(f"/evento/{evento_id}/participante/{new_auth['userId']}?access_token={user_auth['id']}&uid={user_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "User allowed to subscribe another user"

        rv = c.delete(f"/evento/{evento_id}/participante/{user_auth['userId']}?access_token={new_auth['id']}&uid={new_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "User allowed to unsubscribe another user"

        rv = c.delete(f"/evento/{evento_id}/participante/{user_auth['userId']}?access_token={user_auth['id']}&uid={user_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "User prohibited from unsubscribe itself"

        rv = c.delete(f"/evento/{evento_id}/participante/{new_auth['userId']}?access_token={new_auth['id']}&uid={new_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "User prohibited from unsubscribe itself"
