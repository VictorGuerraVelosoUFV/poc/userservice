from random import uniform

from app import app
from http import HTTPStatus

import requests

credentials = {
    "username": "string",
    "password": "string"
}


def gen_string(size):
    import string
    import random
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(size))


def test_administracao_delete():
    another_credentials = {
        "username": "victorgv",
        "password": "string"
    }
    evento_id = int(uniform(5, 1000))
    admin_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items() if key in ['id', 'userId']}

    new_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=another_credentials).json().items() if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.post(f"/evento/{evento_id}/admin/{admin_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "First admin creation asking for permissions it should not ask for"

        rv = c.post(f"/evento/{evento_id}/admin/{new_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "New admin creation disallowed to a proper admin user"

        rv = c.delete(f"/evento/{evento_id}/admin/{new_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "Admin removal by admin failed"

        rv = c.delete(f"/evento/{evento_id}/admin/{admin_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Removal of the last admin allowed"

        rv = c.post(f"/evento/{evento_id}/admin/{new_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "New admin creation disallowed to a proper admin user"

        rv = c.delete(f"/evento/{evento_id}/admin/{admin_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "Admin self deletion prohibited"

        rv = c.delete(f"/evento/{evento_id}?access_token={new_auth['id']}&uid={new_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK


def test_administracao_fake_auth():
    evento_id = int(uniform(5, 1000))
    fake_auth = {'id': gen_string(64), 'userId': gen_string(36)}
    with app.test_client() as c:
        rv = c.get(f"/evento/{evento_id}/admin?access_token={fake_auth['id']}&uid={fake_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Allowed getting information of a non-existent event and user"

        rv = c.post(f"/evento/{evento_id}/admin/{fake_auth['userId']}?access_token={fake_auth['id']}&uid={fake_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "First admin creation allowed for non-existent user"


def test_administracao_create_request():
    another_credentials = {
        "username": "victorgv",
        "password": "string"
    }

    evento_id = int(uniform(5, 1000))
    admin_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=credentials).json().items() if key in ['id', 'userId']}

    new_auth = {key: value for key, value in requests.post("http://poc.primary157.com.br:82/api/auth_users/login", json=another_credentials).json().items() if key in ['id', 'userId']}
    with app.test_client() as c:
        rv = c.get(f"/evento/{evento_id}/admin?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "Allowed getting information of a non-existent event"

        rv = c.post(f"/evento/{evento_id}/admin/{admin_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "First admin creation asking for permissions it should not ask for"

        rv = c.get(f"/evento/{evento_id}/admin?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "Admin not allowed to get information of its event"
        n_admins = len(rv.get_json())
        assert n_admins != 0, "The event's admin is not properly counted"
        assert n_admins == 1, "This event has more admins than it should"

        rv = c.post(f"/evento/{evento_id}/admin/{new_auth['userId']}?access_token={new_auth['id']}&uid={new_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.UNAUTHORIZED, "New admin creation allowed to a non-admin user"

        rv = c.get(f"/evento/{evento_id}/admin?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "Admin not allowed to get information of its event"
        assert n_admins == len(rv.get_json()), "The event's admin count changed when it shouldn't"

        rv = c.post(f"/evento/{evento_id}/admin/{new_auth['userId']}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "New admin creation disallowed to a proper admin user"

        rv = c.get(f"/evento/{evento_id}/admin?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK, "Admin not allowed to get information of its event"
        assert n_admins + 1 == len(rv.get_json()), "The new event's admin is not properly counted"

        rv = c.delete(f"/evento/{evento_id}?access_token={admin_auth['id']}&uid={admin_auth['userId']}", follow_redirects=True)
        assert rv.status_code == HTTPStatus.OK
